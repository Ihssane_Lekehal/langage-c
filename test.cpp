#include <stdio.h> /*Autorise l'emploi de printf et de scanf.*/

int main(void)
{
    int x;
    printf("Entrez un nombre : ");
    scanf("%d", &x);    
    printf("\nLa valeur que vous avez entrer est : %d.\n",x);
    return 0;
}